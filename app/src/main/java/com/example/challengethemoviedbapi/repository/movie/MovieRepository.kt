package com.example.challengethemoviedbapi.repository.movie

import com.example.challengethemoviedbapi.data.datasource.model.Movies

interface MovieRepository {
    suspend fun getUpComingMovies(): Movies
    suspend fun getTopRatedMovies(): Movies
    suspend fun getPopularMovies(): Movies
}