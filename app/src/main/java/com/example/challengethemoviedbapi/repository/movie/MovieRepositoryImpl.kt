package com.example.challengethemoviedbapi.repository.movie

import com.example.challengethemoviedbapi.core.InternetCheck
import com.example.challengethemoviedbapi.data.datalocal.local.LocalMovieDataSource
import com.example.challengethemoviedbapi.data.datasource.source.RemoteMovieDataSource
import com.example.challengethemoviedbapi.data.datasource.model.Movies
import com.example.challengethemoviedbapi.data.datasource.model.toMovieEntity

class MovieRepositoryImpl(
    private val dataSourceRemote: RemoteMovieDataSource,
    private val dataSourceLocal: LocalMovieDataSource
    ): MovieRepository {

    override suspend fun getUpComingMovies(): Movies {
        return if(InternetCheck.isNetworkAvailable()){
            dataSourceRemote.getUpComingMovies().results.forEach { movie ->
                dataSourceLocal.saveMovie(movie.toMovieEntity("upcoming"))
            }
            dataSourceLocal.getUpComingMovies()
        } else {
            dataSourceLocal.getUpComingMovies()
        }
    }

    override suspend fun getTopRatedMovies(): Movies{
        return if(InternetCheck.isNetworkAvailable()){
            dataSourceRemote.getTopRatedMovies().results.forEach { movie ->
                dataSourceLocal.saveMovie(movie.toMovieEntity("toprated"))
            }
            dataSourceLocal.getTopRatedMovies()
        } else {
            dataSourceLocal.getTopRatedMovies()
        }
    }

    override suspend fun getPopularMovies(): Movies {
        return if(InternetCheck.isNetworkAvailable()){
            dataSourceRemote.getPopularMovies().results.forEach { movie ->
                dataSourceLocal.saveMovie(movie.toMovieEntity("popular"))
            }
            dataSourceLocal.getPopularMovies()
        } else {
            dataSourceLocal.getPopularMovies()
        }
    }
}