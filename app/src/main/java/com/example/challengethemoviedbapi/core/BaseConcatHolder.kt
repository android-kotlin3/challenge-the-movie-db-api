package com.example.challengethemoviedbapi.core

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class BaseConcatHolder<T>(itemVew: View): RecyclerView.ViewHolder(itemVew) {
    abstract fun bind(adapter: T)
}