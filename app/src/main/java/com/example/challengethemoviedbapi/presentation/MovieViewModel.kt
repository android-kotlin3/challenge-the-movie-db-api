package com.example.challengethemoviedbapi.presentation

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.liveData
import com.example.challengethemoviedbapi.core.Resource
import com.example.challengethemoviedbapi.repository.movie.MovieRepository
import com.example.challengethemoviedbapi.repository.movie.MovieRepositoryImpl
import kotlinx.coroutines.Dispatchers
import java.lang.Exception

class MovieViewModel(private val repo: MovieRepository): ViewModel() {

    /*
    * Dispatchers will allow me to interact with the Threads of execution,
    * which can have higher priority, higher execution performance,
    * execution in the background or even execute it in the main thread of execution
    * which is the Main Thread
    *
    * As it is in the following case, we need it to run in the background,
    * because we must wait for the request from the server
    * */
    fun fetchMainScreenMovies() = liveData(Dispatchers.IO) {
        emit(Resource.Loading())
        try {
            emit(Resource.Success(
                Triple(repo.getUpComingMovies(), repo.getTopRatedMovies(), repo.getPopularMovies())
            ))
        } catch (e: Exception) {
            emit(Resource.Failure(e))
        }
    }

}

class MovieViewModelFactory(private val repo: MovieRepositoryImpl): ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return modelClass.getConstructor(MovieRepository::class.java).newInstance(repo)
    }
}