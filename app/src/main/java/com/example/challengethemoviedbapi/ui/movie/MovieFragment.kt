package com.example.challengethemoviedbapi.ui.movie

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ConcatAdapter
import com.example.challengethemoviedbapi.R
import com.example.challengethemoviedbapi.core.Resource
import com.example.challengethemoviedbapi.data.datalocal.AppDatabase
import com.example.challengethemoviedbapi.data.datalocal.local.LocalMovieDataSource
import com.example.challengethemoviedbapi.data.datasource.source.RemoteMovieDataSource
import com.example.challengethemoviedbapi.data.datasource.model.Movie
import com.example.challengethemoviedbapi.databinding.FragmentMovieBinding
import com.example.challengethemoviedbapi.presentation.MovieViewModel
import com.example.challengethemoviedbapi.presentation.MovieViewModelFactory
import com.example.challengethemoviedbapi.repository.RetrofitClient
import com.example.challengethemoviedbapi.repository.movie.MovieRepositoryImpl
import com.example.challengethemoviedbapi.ui.movie.adapters.MovieAdapter
import com.example.challengethemoviedbapi.ui.movie.adapters.concat.PopularConcatAdapter
import com.example.challengethemoviedbapi.ui.movie.adapters.concat.TopRatedConcatAdapter
import com.example.challengethemoviedbapi.ui.movie.adapters.concat.UpcomingConcatAdapter

class MovieFragment : Fragment(R.layout.fragment_movie) , MovieAdapter.OnMovieClickListener {

    private lateinit var binding: FragmentMovieBinding

    private val viewModel by viewModels<MovieViewModel> {
        MovieViewModelFactory(
            MovieRepositoryImpl(
                RemoteMovieDataSource(RetrofitClient.webservice),
                LocalMovieDataSource(AppDatabase.getDatabase(requireContext()).movieDao())
            )
        )
    }

    private lateinit var concatAdapter: ConcatAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMovieBinding.bind(view)
        concatAdapter = ConcatAdapter()
        getMainViewModelData()
    }

    private fun getMainViewModelData() {
        viewModel.fetchMainScreenMovies().observe(viewLifecycleOwner , Observer { result ->
            when(result) {
                is Resource.Loading -> {
//                    Log.d("LiveData" , "Loading ...")
                    binding.progressBar.visibility = View.VISIBLE
                }
                is Resource.Success -> {
//                    Log.d("LiveData" , "Upcoming  ${result.data.first}")
//                    Log.d("LiveData" , "TopRated  ${result.data.second}")
//                    Log.d("LiveData" , "Popular  ${result.data.third}")

                    concatAdapter.apply {
                        addAdapter(0, UpcomingConcatAdapter(MovieAdapter(result.data.first.results, this@MovieFragment)))
                        addAdapter(1, TopRatedConcatAdapter(MovieAdapter(result.data.second.results, this@MovieFragment)))
                        addAdapter(2, PopularConcatAdapter(MovieAdapter(result.data.third.results, this@MovieFragment)))
                    }
                    binding.rvMovies.adapter = concatAdapter
                    binding.progressBar.visibility = View.GONE
                }
                is Resource.Failure -> {
//                    Log.d("LiveData" , "Exception ...")
                    binding.progressBar.visibility = View.GONE
                }
            }
        })
    }

    override fun onMovieClick(movie: Movie) {
//        Log.d("Movie", "onMovieClick: ${movie.toString()}")
        val action = MovieFragmentDirections.actionMovieFragmentToMovieDetailFragment(
            posterImageUrl = movie.posterPath,
            backgroundImageUrl = movie.backdropPath,
            voteAverage = movie.voteAverage.toFloat(),
            voteCount = movie.voteCount,
            title = movie.title,
            overview = movie.overview,
            language = movie.originalLanguage,
            releasedDate = movie.releaseDate
        )
        findNavController().navigate(action)
    }

}