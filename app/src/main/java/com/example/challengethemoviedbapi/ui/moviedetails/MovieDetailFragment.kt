package com.example.challengethemoviedbapi.ui.moviedetails

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.challengethemoviedbapi.R
import com.example.challengethemoviedbapi.application.AppConstants
import com.example.challengethemoviedbapi.databinding.FragmentMovieDetailBinding

class MovieDetailFragment : Fragment(R.layout.fragment_movie_detail) {

    private lateinit var binding: FragmentMovieDetailBinding
    private val args by navArgs<MovieDetailFragmentArgs>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentMovieDetailBinding.bind(view)
        loadParameters()
    }

    private fun loadParameters() {
        Glide.with(requireContext()).load("${AppConstants.IMAGE_BASE}${args.posterImageUrl}").centerCrop()
            .into(binding.ivImgMovie)
        Glide.with(requireContext()).load("${AppConstants.IMAGE_BASE}${args.backgroundImageUrl}").centerCrop()
            .into(binding.ivImgBackground)

        binding.tvDescription.text = args.overview
        binding.tvNameMovie.text = args.title
        binding.tvLanguage.text = "Language ${args.language}"
        binding.tvRating.text = "${args.voteAverage} ${args.voteCount} Reviews"
        binding.tvReleased.text = "Released ${args.releasedDate}"
    }

}