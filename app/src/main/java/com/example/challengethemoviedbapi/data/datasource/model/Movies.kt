package com.example.challengethemoviedbapi.data.datasource.model

import com.example.challengethemoviedbapi.data.datasource.model.Movie
import com.google.gson.annotations.SerializedName

data class Movies(
    @SerializedName("results")
    val results: List<Movie> = listOf()
)
