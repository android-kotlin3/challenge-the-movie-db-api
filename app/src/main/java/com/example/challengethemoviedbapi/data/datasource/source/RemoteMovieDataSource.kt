package com.example.challengethemoviedbapi.data.datasource.source

import com.example.challengethemoviedbapi.application.AppConstants
import com.example.challengethemoviedbapi.data.datasource.model.Movies
import com.example.challengethemoviedbapi.repository.WebService

class RemoteMovieDataSource(private val webService: WebService) {
    suspend fun getUpComingMovies(): Movies = webService.getUpComingMovies(AppConstants.API_KEY)
    suspend fun getTopRatedMovies(): Movies = webService.getTopRatedMovies(AppConstants.API_KEY)
    suspend fun getPopularMovies(): Movies = webService.getPopularMovies(AppConstants.API_KEY )
}