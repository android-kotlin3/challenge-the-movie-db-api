package com.example.challengethemoviedbapi.data.datalocal.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.challengethemoviedbapi.data.datasource.model.Movie
import com.example.challengethemoviedbapi.data.datasource.model.Movies

@Entity
data class MovieEntity(
    @PrimaryKey
    val id: Int = -1,
    @ColumnInfo(name = "adult")
    val adult: Boolean = false,
    @ColumnInfo(name = "backdropPath")
    val backdropPath: String = "",
    @ColumnInfo(name = "originalTitle")
    val originalTitle: String = "",
    @ColumnInfo(name = "originalLanguage")
    val originalLanguage: String = "",
    @ColumnInfo(name = "overview")
    val overview: String = "",
    @ColumnInfo(name = "popularity")
    val popularity: Double = -1.0,
    @ColumnInfo(name = "posterPath")
    val posterPath: String = "",
    @ColumnInfo(name = "releaseDate")
    val releaseDate: String = "",
    @ColumnInfo(name = "title")
    val title: String = "",
    @ColumnInfo(name = "video")
    val video: String = "",
    @ColumnInfo(name = "voteAverage")
    val voteAverage: Double = -1.0,
    @ColumnInfo(name = "voteCount")
    val voteCount: Int = -1,
    @ColumnInfo(name = "movie_type")
    val movieType: String = ""
)

// What I need now is to convert from the list that I receive to API to a list of type Database

// First Map Values from MovieEntity to Movie
fun MovieEntity.toMovie() = Movie(
    this.id,
    this.adult,
    this.backdropPath,
    this.originalTitle,
    this.originalLanguage,
    this.overview,
    this.popularity,
    this.posterPath,
    this.releaseDate,
    this.title,
    this.video,
    this.voteAverage,
    this.voteCount,
    this.movieType
)

// Extension Function
fun List<MovieEntity>.toMovieList(): Movies {
    val resultList = mutableListOf<Movie>()
    this.forEach { movieEntity ->
        resultList.add(movieEntity.toMovie())
    }
    return Movies(resultList)
}


