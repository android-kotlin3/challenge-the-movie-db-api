package com.example.challengethemoviedbapi.data.datalocal.local

import com.example.challengethemoviedbapi.data.datasource.model.Movies
import com.example.challengethemoviedbapi.data.datalocal.dao.MovieDao
import com.example.challengethemoviedbapi.data.datalocal.model.MovieEntity
import com.example.challengethemoviedbapi.data.datalocal.model.toMovieList

class LocalMovieDataSource(private val movieDao: MovieDao) {
    suspend fun getUpComingMovies(): Movies {
        return movieDao.getAllMovies().filter { it.movieType == "upcoming" }.toMovieList()
    }
    suspend fun getTopRatedMovies(): Movies {
        return movieDao.getAllMovies().filter { it.movieType == "toprated" }.toMovieList()
    }
    suspend fun getPopularMovies(): Movies {
        return movieDao.getAllMovies().filter { it.movieType == "popular" }.toMovieList()
    }
    suspend fun saveMovie(movie: MovieEntity) {
        movieDao.saveMovie(movie)
    }

}