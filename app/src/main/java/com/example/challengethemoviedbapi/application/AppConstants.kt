package com.example.challengethemoviedbapi.application

object AppConstants {
    // API
    const val BASE_URL = "https://api.themoviedb.org/3/"
    // API KEY
    const val API_KEY = "d233eb0a2c528f1943e3054380e426c2"
    // IMAGE BASE
    const val IMAGE_BASE = "https://image.tmdb.org/t/p/w500"
}